package development;

import org.testng.annotations.Test;

public class TestCases extends BaseTestClass{
	
	@Test //you can add data driven and different anotation like priority etc.
	public void scenario1() {
		
		HomePage homePage=new HomePage();
		
		homePage.restaurantsSearch.click();
		homePage.textBoxSearch.sendKeys("indian");
		homePage.searchResultContainer.wait(2000);
		homePage.searchResultContainer.get(1).click();
		
		SearchResultPage searchResultPage=new SearchResultPage();
		searchResultPage.waitForPageToLoad();
		
		searchResultPage.searchResultList.get(1).isDisplayed();
	}
	
	@Test//you can add data driven and different anotation like priority etc.
	public void scenario2() {
		
		HomePage homePage=new HomePage();
		homePage.signUpLogo.click();
		
		AccountProfilePage accountProfilePage=new AccountProfilePage();
		accountProfilePage.imageProfile.click();
		
		AccountFoodJourneyPage accountFoodJourneyPage=new AccountFoodJourneyPage();
		accountFoodJourneyPage.buttonEditProfile.click();
		
		EditProfilePage editProfilePage=new EditProfilePage();
		editProfilePage.profilePhotoChange.click();
		editProfilePage.optionChangePhoto.click();
		
		editProfilePage.uploadFile("C:\\Users\\pravin.dhage\\Desktop\\Required document in small size\\Photo Copy.jpg"); //file location need to specify
		Thread.sleep(2000);
		
		editProfilePage.messageSuccessfullyProfilechange.isDisplayed();
		System.out.println("Successfully Profile picture changed");
		
	}

}
