package tests;

import java.net.MalformedURLException;
import java.net.URL;

import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeTest;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.remote.MobileCapabilityType;

public class BaseTestClass {

	AppiumDriver<MobileElement> driver;

	@BeforeTest
	public void setup() throws MalformedURLException {

		try {
			DesiredCapabilities capabilities = new DesiredCapabilities();
			capabilities.setCapability(CapabilityType.PLATFORM_NAME, "ANDROID");
			capabilities.setCapability(CapabilityType.VERSION, "");
			capabilities.setCapability(MobileCapabilityType.DEVICE_NAME, "");
			capabilities.setCapability(MobileCapabilityType.UDID, "");
			capabilities.setCapability(MobileCapabilityType.NEW_COMMAND_TIMEOUT, 60);
			capabilities.setCapability(MobileCapabilityType.APP, "");
			capabilities.setCapability("appPackage", "com.android.zomato");
			capabilities.setCapability("appActivity", "com.android.zomato2.zomato");
			// caps.setCapability(MobileCapabilityType.BROWSER_NAME, ""); This is for mobile browser

			URL url = new URL("server ip address need to add here");
			driver = new AppiumDriver<MobileElement>(url, capabilities);

		} catch (Exception exp) {

			System.out.println("Cause is" + exp.getCause());
			System.out.println("Cause is" + exp.getMessage());
			exp.printStackTrace();
		}

	}

	@AfterSuite
	public void teardown() {
		driver.quit();
	}

}
