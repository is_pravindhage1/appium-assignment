import java.util.List;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class HomePage{
	
	@FindBy(xpath="//img[@class='zimage shown zimage-loaded']")
	WebElement zomatoLogo;
	
	@FindBy(xpath="//div[@class='profile_icon rippleContainer']/img")
	WebElement signUpLogo;
	
	@FindBy(xpath="//div[@class='profile_icon rippleContainer']/img")
	WebElement restaurantsSearch;
	
	@FindBy(xpath="//input[@name='restaurant']")
	WebElement textBoxSearch;
	
	@FindBy(xpath="//div[@class='result__container']")
	List<WebElement> searchResultContainer;
	
	

}
