import java.util.List;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class SearchResultPage {
	
	@FindBy(xpath="//h1[contains(@class,'search_heading')]")
	WebElement headerSearchResult;
	
	@FindBy(xpath="//div[@class='result__container']")
	List<WebElement> searchResultList;
	
	public void waitForPageToLoad() {
		headerSearchResult.isEnabled();
		headerSearchResult.isDisplayed();
	}

}
